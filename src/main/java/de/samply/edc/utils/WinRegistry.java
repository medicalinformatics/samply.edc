/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.utils;

/**
 * Pure Java Windows Registry access.
 * Modified by petrucio@stackoverflow(828681) to add support for
 * reading (and writing but not creating/deleting keys) the 32-bits
 * registry view from a 64-bits JVM (KEY_WOW64_32KEY)
 * and 64-bits view from a 32-bits JVM (KEY_WOW64_64KEY).
 *****************************************************************************/

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

/**
 * The Class WinRegistry.
 */
public class WinRegistry {
    
    /** The Constant HKEY_CURRENT_USER. */
    public static final int HKEY_CURRENT_USER = 0x80000001;
    
    /** The Constant HKEY_LOCAL_MACHINE. */
    public static final int HKEY_LOCAL_MACHINE = 0x80000002;
    
    /** The Constant REG_SUCCESS. */
    public static final int REG_SUCCESS = 0;
    
    /** The Constant REG_NOTFOUND. */
    public static final int REG_NOTFOUND = 2;
    
    /** The Constant REG_ACCESSDENIED. */
    public static final int REG_ACCESSDENIED = 5;

    /** The Constant KEY_WOW64_32KEY. */
    public static final int KEY_WOW64_32KEY = 0x0200;
    
    /** The Constant KEY_WOW64_64KEY. */
    public static final int KEY_WOW64_64KEY = 0x0100;

    /** The Constant KEY_ALL_ACCESS. */
    private static final int KEY_ALL_ACCESS = 0xf003f;
    
    /** The Constant KEY_READ. */
    private static final int KEY_READ = 0x20019;
    
    /** The user root. */
    private static Preferences userRoot = Preferences.userRoot();
    
    /** The system root. */
    private static Preferences systemRoot = Preferences.systemRoot();
    
    /** The user class. */
    private static Class<? extends Preferences> userClass = userRoot.getClass();
    
    /** The reg open key. */
    private static Method regOpenKey = null;
    
    /** The reg close key. */
    private static Method regCloseKey = null;
    
    /** The reg query value ex. */
    private static Method regQueryValueEx = null;
    
    /** The reg enum value. */
    private static Method regEnumValue = null;
    
    /** The reg query info key. */
    private static Method regQueryInfoKey = null;
    
    /** The reg enum key ex. */
    private static Method regEnumKeyEx = null;
    
    /** The reg create key ex. */
    private static Method regCreateKeyEx = null;
    
    /** The reg set value ex. */
    private static Method regSetValueEx = null;
    
    /** The reg delete key. */
    private static Method regDeleteKey = null;
    
    /** The reg delete value. */
    private static Method regDeleteValue = null;

    static {
        try {
            regOpenKey = userClass.getDeclaredMethod("WindowsRegOpenKey",
                    new Class[] { int.class, byte[].class, int.class });
            regOpenKey.setAccessible(true);
            regCloseKey = userClass.getDeclaredMethod("WindowsRegCloseKey",
                    new Class[] { int.class });
            regCloseKey.setAccessible(true);
            regQueryValueEx = userClass.getDeclaredMethod(
                    "WindowsRegQueryValueEx", new Class[] { int.class,
                            byte[].class });
            regQueryValueEx.setAccessible(true);
            regEnumValue = userClass.getDeclaredMethod("WindowsRegEnumValue",
                    new Class[] { int.class, int.class, int.class });
            regEnumValue.setAccessible(true);
            regQueryInfoKey = userClass.getDeclaredMethod(
                    "WindowsRegQueryInfoKey1", new Class[] { int.class });
            regQueryInfoKey.setAccessible(true);
            regEnumKeyEx = userClass.getDeclaredMethod("WindowsRegEnumKeyEx",
                    new Class[] { int.class, int.class, int.class });
            regEnumKeyEx.setAccessible(true);
            regCreateKeyEx = userClass.getDeclaredMethod(
                    "WindowsRegCreateKeyEx", new Class[] { int.class,
                            byte[].class });
            regCreateKeyEx.setAccessible(true);
            regSetValueEx = userClass.getDeclaredMethod("WindowsRegSetValueEx",
                    new Class[] { int.class, byte[].class, byte[].class });
            regSetValueEx.setAccessible(true);
            regDeleteValue = userClass.getDeclaredMethod(
                    "WindowsRegDeleteValue", new Class[] { int.class,
                            byte[].class });
            regDeleteValue.setAccessible(true);
            regDeleteKey = userClass.getDeclaredMethod("WindowsRegDeleteKey",
                    new Class[] { int.class, byte[].class });
            regDeleteKey.setAccessible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Instantiates a new win registry.
     */
    private WinRegistry() {
    }

    /**
     * Read a value from key and value name.
     *
     * @param hkey            HKEY_CURRENT_USER/HKEY_LOCAL_MACHINE
     * @param key the key
     * @param valueName the value name
     * @param wow64            0 for standard registry access (32-bits for 32-bit app,
     *            64-bits for 64-bits app) or KEY_WOW64_32KEY to force access to
     *            32-bit registry view, or KEY_WOW64_64KEY to force access to
     *            64-bit registry view
     * @return the value
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    public static String readString(int hkey, String key, String valueName,
            int wow64) throws IllegalArgumentException, IllegalAccessException,
            InvocationTargetException {
        if (hkey == HKEY_LOCAL_MACHINE) {
            return readString(systemRoot, hkey, key, valueName, wow64);
        } else if (hkey == HKEY_CURRENT_USER) {
            return readString(userRoot, hkey, key, valueName, wow64);
        } else {
            throw new IllegalArgumentException("hkey=" + hkey);
        }
    }

    /**
     * Read value(s) and value name(s) form given key.
     *
     * @param hkey            HKEY_CURRENT_USER/HKEY_LOCAL_MACHINE
     * @param key the key
     * @param wow64            0 for standard registry access (32-bits for 32-bit app,
     *            64-bits for 64-bits app) or KEY_WOW64_32KEY to force access to
     *            32-bit registry view, or KEY_WOW64_64KEY to force access to
     *            64-bit registry view
     * @return the value name(s) plus the value(s)
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    public static Map<String, String> readStringValues(int hkey, String key,
            int wow64) throws IllegalArgumentException, IllegalAccessException,
            InvocationTargetException {
        if (hkey == HKEY_LOCAL_MACHINE) {
            return readStringValues(systemRoot, hkey, key, wow64);
        } else if (hkey == HKEY_CURRENT_USER) {
            return readStringValues(userRoot, hkey, key, wow64);
        } else {
            throw new IllegalArgumentException("hkey=" + hkey);
        }
    }

    /**
     * Read the value name(s) from a given key.
     *
     * @param hkey            HKEY_CURRENT_USER/HKEY_LOCAL_MACHINE
     * @param key the key
     * @param wow64            0 for standard registry access (32-bits for 32-bit app,
     *            64-bits for 64-bits app) or KEY_WOW64_32KEY to force access to
     *            32-bit registry view, or KEY_WOW64_64KEY to force access to
     *            64-bit registry view
     * @return the value name(s)
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    public static List<String> readStringSubKeys(int hkey, String key, int wow64)
            throws IllegalArgumentException, IllegalAccessException,
            InvocationTargetException {
        if (hkey == HKEY_LOCAL_MACHINE) {
            return readStringSubKeys(systemRoot, hkey, key, wow64);
        } else if (hkey == HKEY_CURRENT_USER) {
            return readStringSubKeys(userRoot, hkey, key, wow64);
        } else {
            throw new IllegalArgumentException("hkey=" + hkey);
        }
    }

    /**
     * Create a key.
     *
     * @param hkey            HKEY_CURRENT_USER/HKEY_LOCAL_MACHINE
     * @param key the key
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    public static void createKey(int hkey, String key)
            throws IllegalArgumentException, IllegalAccessException,
            InvocationTargetException {
        int[] ret;
        if (hkey == HKEY_LOCAL_MACHINE) {
            ret = createKey(systemRoot, hkey, key);
            regCloseKey
                    .invoke(systemRoot, new Object[] { new Integer(ret[0]) });
        } else if (hkey == HKEY_CURRENT_USER) {
            ret = createKey(userRoot, hkey, key);
            regCloseKey.invoke(userRoot, new Object[] { new Integer(ret[0]) });
        } else {
            throw new IllegalArgumentException("hkey=" + hkey);
        }
        if (ret[1] != REG_SUCCESS) {
            throw new IllegalArgumentException("rc=" + ret[1] + "  key=" + key);
        }
    }

    /**
     * Write a value in a given key/value name.
     *
     * @param hkey the hkey
     * @param key the key
     * @param valueName the value name
     * @param value the value
     * @param wow64            0 for standard registry access (32-bits for 32-bit app,
     *            64-bits for 64-bits app) or KEY_WOW64_32KEY to force access to
     *            32-bit registry view, or KEY_WOW64_64KEY to force access to
     *            64-bit registry view
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    public static void writeStringValue(int hkey, String key, String valueName,
            String value, int wow64) throws IllegalArgumentException,
            IllegalAccessException, InvocationTargetException {
        if (hkey == HKEY_LOCAL_MACHINE) {
            writeStringValue(systemRoot, hkey, key, valueName, value, wow64);
        } else if (hkey == HKEY_CURRENT_USER) {
            writeStringValue(userRoot, hkey, key, valueName, value, wow64);
        } else {
            throw new IllegalArgumentException("hkey=" + hkey);
        }
    }

    /**
     * Delete a given key.
     *
     * @param hkey the hkey
     * @param key the key
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    public static void deleteKey(int hkey, String key)
            throws IllegalArgumentException, IllegalAccessException,
            InvocationTargetException {
        int rc = -1;
        if (hkey == HKEY_LOCAL_MACHINE) {
            rc = deleteKey(systemRoot, hkey, key);
        } else if (hkey == HKEY_CURRENT_USER) {
            rc = deleteKey(userRoot, hkey, key);
        }
        if (rc != REG_SUCCESS) {
            throw new IllegalArgumentException("rc=" + rc + "  key=" + key);
        }
    }

    /**
     * delete a value from a given key/value name.
     *
     * @param hkey the hkey
     * @param key the key
     * @param value the value
     * @param wow64            0 for standard registry access (32-bits for 32-bit app,
     *            64-bits for 64-bits app) or KEY_WOW64_32KEY to force access to
     *            32-bit registry view, or KEY_WOW64_64KEY to force access to
     *            64-bit registry view
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    public static void deleteValue(int hkey, String key, String value, int wow64)
            throws IllegalArgumentException, IllegalAccessException,
            InvocationTargetException {
        int rc = -1;
        if (hkey == HKEY_LOCAL_MACHINE) {
            rc = deleteValue(systemRoot, hkey, key, value, wow64);
        } else if (hkey == HKEY_CURRENT_USER) {
            rc = deleteValue(userRoot, hkey, key, value, wow64);
        }
        if (rc != REG_SUCCESS) {
            throw new IllegalArgumentException("rc=" + rc + "  key=" + key
                    + "  value=" + value);
        }
    }

    /**
     * Delete value.
     *
     * @param root the root
     * @param hkey the hkey
     * @param key the key
     * @param value the value
     * @param wow64 the wow64
     * @return the int
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    // ========================================================================
    private static int deleteValue(Preferences root, int hkey, String key,
            String value, int wow64) throws IllegalArgumentException,
            IllegalAccessException, InvocationTargetException {
        int[] handles = (int[]) regOpenKey.invoke(root, new Object[] {
                new Integer(hkey), toCstr(key),
                new Integer(KEY_ALL_ACCESS | wow64) });
        if (handles[1] != REG_SUCCESS) {
            return handles[1]; // can be REG_NOTFOUND, REG_ACCESSDENIED
        }
        int rc = ((Integer) regDeleteValue.invoke(root, new Object[] {
                new Integer(handles[0]), toCstr(value) })).intValue();
        regCloseKey.invoke(root, new Object[] { new Integer(handles[0]) });
        return rc;
    }

    /**
     * Delete key.
     *
     * @param root the root
     * @param hkey the hkey
     * @param key the key
     * @return the int
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    // ========================================================================
    private static int deleteKey(Preferences root, int hkey, String key)
            throws IllegalArgumentException, IllegalAccessException,
            InvocationTargetException {
        int rc = ((Integer) regDeleteKey.invoke(root, new Object[] {
                new Integer(hkey), toCstr(key) })).intValue();
        return rc; // can REG_NOTFOUND, REG_ACCESSDENIED, REG_SUCCESS
    }

    /**
     * Read string.
     *
     * @param root the root
     * @param hkey the hkey
     * @param key the key
     * @param value the value
     * @param wow64 the wow64
     * @return the string
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    // ========================================================================
    private static String readString(Preferences root, int hkey, String key,
            String value, int wow64) throws IllegalArgumentException,
            IllegalAccessException, InvocationTargetException {
        int[] handles = (int[]) regOpenKey
                .invoke(root, new Object[] { new Integer(hkey), toCstr(key),
                        new Integer(KEY_READ | wow64) });
        if (handles[1] != REG_SUCCESS) {
            return null;
        }
        byte[] valb = (byte[]) regQueryValueEx.invoke(root, new Object[] {
                new Integer(handles[0]), toCstr(value) });
        regCloseKey.invoke(root, new Object[] { new Integer(handles[0]) });
        return (valb != null ? new String(valb).trim() : null);
    }

    /**
     * Read string values.
     *
     * @param root the root
     * @param hkey the hkey
     * @param key the key
     * @param wow64 the wow64
     * @return the map
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    // ========================================================================
    private static Map<String, String> readStringValues(Preferences root,
            int hkey, String key, int wow64) throws IllegalArgumentException,
            IllegalAccessException, InvocationTargetException {
        HashMap<String, String> results = new HashMap<String, String>();
        int[] handles = (int[]) regOpenKey
                .invoke(root, new Object[] { new Integer(hkey), toCstr(key),
                        new Integer(KEY_READ | wow64) });
        if (handles[1] != REG_SUCCESS) {
            return null;
        }
        int[] info = (int[]) regQueryInfoKey.invoke(root,
                new Object[] { new Integer(handles[0]) });

        int count = info[2]; // count
        int maxlen = info[3]; // value length max
        for (int index = 0; index < count; index++) {
            byte[] name = (byte[]) regEnumValue.invoke(root, new Object[] {
                    new Integer(handles[0]), new Integer(index),
                    new Integer(maxlen + 1) });
            String value = readString(hkey, key, new String(name), wow64);
            results.put(new String(name).trim(), value);
        }
        regCloseKey.invoke(root, new Object[] { new Integer(handles[0]) });
        return results;
    }

    /**
     * Read string sub keys.
     *
     * @param root the root
     * @param hkey the hkey
     * @param key the key
     * @param wow64 the wow64
     * @return the list
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    // ========================================================================
    private static List<String> readStringSubKeys(Preferences root, int hkey,
            String key, int wow64) throws IllegalArgumentException,
            IllegalAccessException, InvocationTargetException {
        List<String> results = new ArrayList<String>();
        int[] handles = (int[]) regOpenKey
                .invoke(root, new Object[] { new Integer(hkey), toCstr(key),
                        new Integer(KEY_READ | wow64) });
        if (handles[1] != REG_SUCCESS) {
            return null;
        }
        int[] info = (int[]) regQueryInfoKey.invoke(root,
                new Object[] { new Integer(handles[0]) });

        int count = info[0]; // Fix: info[2] was being used here with wrong
                                // results. Suggested by davenpcj, confirmed by
                                // Petrucio
        int maxlen = info[3]; // value length max
        for (int index = 0; index < count; index++) {
            byte[] name = (byte[]) regEnumKeyEx.invoke(root, new Object[] {
                    new Integer(handles[0]), new Integer(index),
                    new Integer(maxlen + 1) });
            results.add(new String(name).trim());
        }
        regCloseKey.invoke(root, new Object[] { new Integer(handles[0]) });
        return results;
    }

    /**
     * Creates the key.
     *
     * @param root the root
     * @param hkey the hkey
     * @param key the key
     * @return the int[]
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    // ========================================================================
    private static int[] createKey(Preferences root, int hkey, String key)
            throws IllegalArgumentException, IllegalAccessException,
            InvocationTargetException {
        return (int[]) regCreateKeyEx.invoke(root, new Object[] {
                new Integer(hkey), toCstr(key) });
    }

    /**
     * Write string value.
     *
     * @param root the root
     * @param hkey the hkey
     * @param key the key
     * @param valueName the value name
     * @param value the value
     * @param wow64 the wow64
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    // ========================================================================
    private static void writeStringValue(Preferences root, int hkey,
            String key, String valueName, String value, int wow64)
            throws IllegalArgumentException, IllegalAccessException,
            InvocationTargetException {
        int[] handles = (int[]) regOpenKey.invoke(root, new Object[] {
                new Integer(hkey), toCstr(key),
                new Integer(KEY_ALL_ACCESS | wow64) });
        regSetValueEx.invoke(root, new Object[] { new Integer(handles[0]),
                toCstr(valueName), toCstr(value) });
        regCloseKey.invoke(root, new Object[] { new Integer(handles[0]) });
    }

    // ========================================================================
    /**
     * To cstr.
     *
     * @param str the str
     * @return the byte[]
     */
    // utility
    private static byte[] toCstr(String str) {
        byte[] result = new byte[str.length() + 1];

        for (int i = 0; i < str.length(); i++) {
            result[i] = (byte) str.charAt(i);
        }
        result[str.length()] = 0;
        return result;
    }
}
