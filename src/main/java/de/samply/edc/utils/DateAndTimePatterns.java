/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.utils;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.faces.context.FacesContext;

import org.apache.log4j.LogManager;

/**
 *  Date and Time patterns as defined by the MDR
 *  and used in the MDRFaces.
 */
public final class DateAndTimePatterns implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Gets the date format pattern from the MDR date format description.
     * These patterns are predefined in the MDR
     * TODO: Maybe import them from the MDR for future changes, needs an interface on the MDR for that
     *
     * @param enumTimeFormat
     *            the date format as known in MDR
     * @return the date format pattern string representation
     */
    public static String getTimePattern(final String enumTimeFormat) {
        DateFormat formatter;
        switch (enumTimeFormat) {
        case "HOURS_24":
            formatter = new SimpleDateFormat("HH:mm");
            break;
        case "HOURS_24_WITH_SECONDS":
            formatter = new SimpleDateFormat("HH:mm:ss");
            break;
        case "HOURS_12":
            formatter = new SimpleDateFormat("h:mm a");
            break;
        case "HOURS_12_WITH_SECONDS":
            formatter = new SimpleDateFormat("h:mm:ss a");
            break;
        case "LOCAL_TIME":
            formatter = DateFormat.getTimeInstance(DateFormat.SHORT, getLocale());
            break;
        case "LOCAL_TIME_WITH_SECONDS":
            formatter = DateFormat.getTimeInstance(DateFormat.MEDIUM, getLocale());
            break;
        default:
            formatter = new SimpleDateFormat("HH:mm");
            break;
        }
        return ((SimpleDateFormat) formatter).toPattern();
    }

    /**
     * Get the date format pattern from the MDR date format description.
     * These patterns are predefined in the MDR
     * TODO: Maybe import them from the MDR for future changes, needs an interface on the MDR for that
     *
     * @param enumDateFormat
     *            the date format as known in MDR
     * @return the date format pattern string representation
     */
    public static String getDatePattern(final String enumDateFormat) {
        DateFormat formatter;
        switch (enumDateFormat) {
        case "DIN_5008":
            formatter = new SimpleDateFormat("MM.yyyy");
            break;
        case "DIN_5008_WITH_DAYS":
            formatter = new SimpleDateFormat("dd.MM.yyyy");
            break;
        case "ISO_8601":
            formatter = new SimpleDateFormat("yyyy-MM");
            break;
        case "ISO_8601_WITH_DAYS":
            formatter = new SimpleDateFormat("yyyy-MM-dd");
            break;
        case "LOCAL_DATE": // not valid
        case "LOCAL_DATE_WITH_DAYS":
        default:
            formatter = DateFormat.getDateInstance(DateFormat.MEDIUM, getLocale());
            break;
        }
        return ((SimpleDateFormat) formatter).toPattern();
    }

    /**
     * Get the date format pattern from the MDR date format description.
     *
     * @param enumDateFormat
     *            the date format as known in MDR
     * @param enumTimeFormat
     *            the time format as known in the MDR
     * @return the date format pattern string representation
     */
    public static String getDateTimePattern(final String enumDateFormat, final String enumTimeFormat) {
        String datePattern = getDatePattern(enumDateFormat);
        String timePattern = getTimePattern(enumTimeFormat);

        if (!datePattern.isEmpty() && !timePattern.isEmpty()) {
            DateFormat formatter = new SimpleDateFormat(datePattern + " " + timePattern);
            return ((SimpleDateFormat) formatter).toPattern();
        } else {
            LogManager.getLogger(DateAndTimePatterns.class.getName()).warn(
                    "Could not get a formatter for " + enumDateFormat + " " + enumTimeFormat);
            return "";
        }
    }

    /**
     * Gets the locale of the JSF application, and if we have no context, we assume "de" locale.
     *
     * @return the defined locale
     */
    public static Locale getLocale() {
        if (FacesContext.getCurrentInstance() != null) {
            return FacesContext.getCurrentInstance().getViewRoot().getLocale();
        } else {
            return new Locale("de");
        }
    }
}
