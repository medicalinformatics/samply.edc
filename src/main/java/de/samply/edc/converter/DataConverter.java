/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import de.samply.edc.model.DatatableRow;
import de.samply.edc.model.DatatableRows;
import de.samply.edc.utils.DateAndTimePatterns;
import de.samply.edc.utils.Utils;
import de.samply.store.AbstractResource;
import de.samply.store.JSONResource;
import de.samply.store.NumberLiteral;
import de.samply.store.Value;

/**
 * This is a class to convert data (mostly for data tables) to be saved into the
 * backend (as json).
 */
public class DataConverter {

    /**
     * Converts a JSONResource to a datatablerows record representation.
     *
     * @param myJSON the my json
     * @param mdrEntityHasValidation the validation data
     * @return the hash map
     */
    public static HashMap<String, Object> convertJSONToRecord(JSONResource myJSON, JSONResource mdrEntityHasValidation) {

        DatatableRows datatableRows = convertJSONToDatatableRows(myJSON, mdrEntityHasValidation);

        if(datatableRows.getRows().isEmpty())
            return null;

        return datatableRows.getRows().get(0).getColumns();
    }

    /**
     * Converts a record into a JSONResource for saving the data.
     *
     * @param membersAndValues  hashmap of the record
     * @return the JSON resource
     */
    public static JSONResource convertRecordToJSONResource(HashMap<String,Object> membersAndValues) {
        // We store such a nonrepeatable record like a repeatable one with only one row, so we make use of the method below
        DatatableRows datatableRows = new DatatableRows();
        ArrayList<DatatableRow> rows = new ArrayList<DatatableRow>();
        DatatableRow row = new DatatableRow();
        row.setColumns(membersAndValues);
        rows.add(row);
        datatableRows.setRows(rows);

        JSONResource datatableJSON = convertDatatableRowsToJSONResource(datatableRows);
        // remove the datatable marker
        datatableJSON.removeProperties("osse.this.is.datatable");

        // Add a marker to the resource, so we know what to convert it back to
        datatableJSON.addProperty("osse.this.is.nonrepeatablerecord", true);

        return datatableJSON;
    }

    /**
     * Puts a key-value into a hashmap with correct conversion and casting
     * Used in loading of an entity.
     *
     * @param properties    the HashMap
     * @param mdrKey    the MDRkey
     * @param value the value
     * @param mdrEntityHasValidation the validation data
     * @return  the HashMap with the saved key-value
     */
    public static HashMap<String, Object> putKeyValueInHashMap(HashMap<String,Object> properties, String mdrKey, Value value, JSONResource mdrEntityHasValidation) {
        mdrKey = Utils.fixMDRkeyForLoad(mdrKey);

        Date dateTest = convertSecuredTimeStringToDate(value.getValue());
        if (dateTest != null) {
            properties.put(mdrKey, dateTest);
        }
        //TODO: enable this after checking the deeper consequences
//        else if(value instanceof ResourceIdentifierLiteral) {
//            properties.put(str, (ResourceIdentifierLiteral) value);
//        }
        else if (value instanceof NumberLiteral) {
            properties.put(mdrKey, ((NumberLiteral) value).get());
        } else if (value.asBoolean() != null) {
            properties.put(mdrKey, value.asBoolean());
        } else if (value.asJSONResource() != null) {
            // A JSONResource can be an array, a datatablerows, or a
            // JSONResource
            JSONResource moo = value.asJSONResource();

            if (moo.getProperty("osse.this.is.array") != null) {
                ArrayList<String> temp = new ArrayList<String>();
                List<Value> arrayEntries = moo.getProperties("arrayEntry");
                for (Value me : arrayEntries) {
                    temp.add(me.getValue());
                }
                properties.put(mdrKey,
                        temp.toArray(new String[temp.size()]));
            } else if (moo.getProperty("osse.this.is.datatable") != null) {
                properties.put(mdrKey,
                        convertJSONToDatatableRows(moo, mdrEntityHasValidation));
            } else if (moo.getProperty("osse.this.is.nonrepeatablerecord") != null) {
                HashMap<String, Object> temp = convertJSONToRecord(moo, mdrEntityHasValidation);
                for(String recordMemberKey: temp.keySet()) {
                    String combinedKey = mdrKey + "/" + Utils.fixMDRkeyForLoad(recordMemberKey);
                    properties.put(combinedKey, temp.get(recordMemberKey));
                }
            } else {
                properties.put(mdrKey, moo);
            }
        } else {
            properties.put(mdrKey, value.getValue());
        }

        return properties;
    }

    /**
     * provides the mdr validation type and (if available) the date,time,datetime in iso format.
     *
     * @param mdrKey    the mdr Key (purified or not)
     * @param value     the value to check
     * @param mdrEntityHasValidation    the mdrValidationMatrix
     * @return the hash map
     */
    @SuppressWarnings("unused")
    private static HashMap<String,String> provideTypeAndISODateValue(String mdrKey, String value, JSONResource mdrEntityHasValidation) {
        if(value == null || "".equals(value))
            return null;

        if(mdrEntityHasValidation == null)
            return null;

        // if it's "purified" we transform it to a real mdr Key again
        mdrKey = Utils.fixMDRkeyForSave(mdrKey);

        Value validationEntry = mdrEntityHasValidation.getProperty(mdrKey);

        if(validationEntry == null || !(validationEntry instanceof JSONResource))
            return null;

        Value validationType = validationEntry.asJSONResource().getProperty("type");

        if(validationType == null)
            return null;

        HashMap<String,String> map = new HashMap<String, String>();
        map.put("mdr-type", validationType.getValue());

        Value dateFormat = validationEntry.asJSONResource().getProperty("enumDateFormat");
        Value timeFormat = validationEntry.asJSONResource().getProperty("enumTimeFormat");

        String pattern = null;

        if(dateFormat == null) {
            if(timeFormat == null)
                return map;
            else {
                pattern = DateAndTimePatterns.getTimePattern(timeFormat.getValue());
            }
        } else {
            if(timeFormat == null) {
                pattern = DateAndTimePatterns.getDatePattern(dateFormat.getValue());
            } else {
                pattern = DateAndTimePatterns.getDateTimePattern(dateFormat.getValue(), timeFormat.getValue());
            }
        }

        if(pattern == null)
            return map;

        SimpleDateFormat dt = new SimpleDateFormat(pattern);
        try {
            Date date = dt.parse(value);
            SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            map.put("time-utc", isoFormat.format(date));
        } catch (ParseException e) {
            //e.printStackTrace();
        }

        return map;
    }

    /**
     * Puts a key-value into a resource with correct casting or conversion of the value
     * Used in saving an entity.
     *
     * @param resource  The resource the values shall be saved in
     * @param mdrKey   The MDRkey
     * @param value The object value
     * @return  The resource with the saved key-value
     */
    public static AbstractResource putKeyValueInResource(AbstractResource resource, String mdrKey, Object value) {
        if(value == null)
            return resource;

        // convert _-key to :-key
        mdrKey = Utils.fixMDRkeyForSave(mdrKey);

        if (value instanceof Boolean) {
            resource.setProperty(mdrKey, (Boolean) value);
        } else if (value instanceof Number) {
            resource.setProperty(mdrKey, (Number) value);
        } else if (value instanceof Date) {
            resource.setProperty(mdrKey,
                    convertDateToSecuredTimeString((Date) value));
        } else if (value instanceof Value) {
            resource.setProperty(mdrKey, (Value) value);
        }
        else if (value instanceof List) {
            resource.removeProperties(mdrKey);

            for (Object listValue : (Collection<?>) value) {
                if (listValue instanceof Boolean) {
                    resource.addProperty(mdrKey, (Boolean) listValue);
                } else if (listValue instanceof Number) {
                    resource.addProperty(mdrKey, (Number) listValue);
                } else if (value instanceof Date) {
                    resource.addProperty(mdrKey, convertDateToSecuredTimeString((Date) listValue));
                } else if (listValue instanceof Value) {
                    resource.addProperty(mdrKey, (Value) listValue);
                } else if (value instanceof DatatableRows) {
                    JSONResource myValue = convertDatatableRowsToJSONResource((DatatableRows) listValue);
                    resource.addProperty(mdrKey, myValue);
                } else {
                    resource.addProperty(mdrKey, listValue.toString());
                }
            }
        } else if (value.getClass().isArray()) {
            JSONResource arrayJSON = new JSONResource();
            arrayJSON.setProperty("osse.this.is.array", true);
            for (String arrayValue : (String[]) value) {
                arrayJSON.addProperty("arrayEntry", arrayValue);
            }

            resource.setProperty(mdrKey, arrayJSON);
        } else if (value instanceof DatatableRows) {
            JSONResource myValue = convertDatatableRowsToJSONResource((DatatableRows) value);
            resource.setProperty(mdrKey, myValue);
        } else {
            resource.setProperty(mdrKey, value.toString());
        }

        return resource;
    }


    /**
     * Converts our precious Datatables to a JSONResource.
     *
     * @param datatableRows
     *            the data table rows to converted
     * @return the converted data
     */
    public static JSONResource convertDatatableRowsToJSONResource(
            DatatableRows datatableRows) {
        JSONResource datatableJSON = new JSONResource();

        // Add a marker to the resource, so we know what to convert it back to
        datatableJSON.addProperty("osse.this.is.datatable", true);

        // Run through the Rows
        for (DatatableRow row : datatableRows.getRows()) {
            JSONResource colJSON = new JSONResource();

            // Run through the columns of a row
            for (String colKey : row.getColumns().keySet()) {
                Object colValue = row.getColumns().get(colKey);

                colJSON = (JSONResource) putKeyValueInResource(colJSON, colKey, colValue);
            }

            // Store the column into the datatableJSON (key = column)
            datatableJSON.addProperty("column", colJSON);
        }

        return datatableJSON;
    }

    /**
     * Converts a JSONResource back into a DatatableRows object.
     *
     * @param myJSON            JSON representation of the data to convert.
     * @param mdrEntityHasValidation the validation data.
     * @return DatatableRows The converted data.
     */
    public static DatatableRows convertJSONToDatatableRows(JSONResource myJSON, JSONResource mdrEntityHasValidation) {
        DatatableRows datatableRows = new DatatableRows();
        ArrayList<DatatableRow> rows = new ArrayList<DatatableRow>();

        // run through the columns
        for (Value aColumn : myJSON.getProperties("column")) {
            HashMap<String, Object> columns = new HashMap<String, Object>();
            JSONResource colJSON = (JSONResource) aColumn;

            // run through the entries of a column
            for (String colKey : colJSON.getDefinedProperties()) {
                for(Value colValue: colJSON.getProperties(colKey)) {
                    columns = putKeyValueInHashMap(columns, colKey, colValue, mdrEntityHasValidation);
                }
            }

            DatatableRow row = new DatatableRow();
            row.setColumns(columns);
            rows.add(row);
        }

        datatableRows.setRows(rows);

        return datatableRows;
    }




    /**
     * Convert to a german date string, using time zone "Europe/Berlin". Using a
     * fixed time zone is important to display the date correctly worldwide
     *
     * @param date
     *            the date
     * @return the converted date string, formatted as "dd.MM.yyyy"
     */
    public static String convertToGermanDateString(Date date) {
        return convertToDateString(date, "dd.MM.yyyy", "Europe/Berlin");
    }

    /**
     * Convert from german date string, assuming time zone "Europe/Berlin".
     *
     * @param value            a date string in format "dd.MM.yyyy"
     * @return the converted date, or the given date string if conversion failed
     */
    public static Date convertFromGermanDateString(String value) {
        return convertFromDateString(value, "dd.MM.yyyy", "Europe/Berlin");
    }

    /**
     * Convert to german date time string, using time zone "Europe/Berlin".
     * Using a fixed time zone is important to display the date correctly
     * worldwide
     *
     * @param date
     *            the date
     * @return the converted date time string, formatted as "dd.MM.yyyy HH:mm"
     */
    public static String convertToGermanDateTimeString(Date date) {
        return convertToDateString(date, "dd.MM.yyyy HH:mm", "Europe/Berlin");
    }

    /**
     * Convert from german time string, assuming time zone "Europe/Berlin".
     *
     * @param value
     *            a time string in format "dd.MM.yyyy HH:mm"
     * @return the converted date, or the given time string if conversion failed
     */
    public static Date convertFromGermanDateTimeString(String value) {
        return convertFromDateString(value, "dd.MM.yyyy HH:mm", "Europe/Berlin");
    }

    /**
     * Convert to date string, using time zone "Europe/Berlin". Using a fixed
     * time zone is important to display the date correctly worldwide
     *
     * @param date
     *            the date
     * @return the converted date string, formatted as "yyyy-MM-dd"
     */
    public static String convertToISODateString(Date date) {
        return convertToDateString(date, "yyyy-MM-dd", "Europe/Berlin");
    }


    /**
     * Convert to date time string, using time zone "Europe/Berlin". Using a
     * fixed time zone is important to display the date correctly worldwide
     *
     * @param date
     *            the date
     * @return the converted date time string, formatted as "yyyy-MM-dd HH:mm:ss"
     */
    public static String convertToISODateTimeString(Date date) {
        return convertToDateString(date, "yyyy-MM-dd HH:mm:ss", "Europe/Berlin");
    }

    /**
     * Convert from date time string, assuming time zone "Europe/Berlin".
     *
     * @param value            a date time string in format "yyyy-MM-dd HH:mm:ss"
     * @return the converted date, or the given date time string if conversion
     *         failed
     */
    public static Date convertFromISODateTimeString(String value) {
        return convertFromDateString(value, "yyyy-MM-dd HH:mm:ss", "Europe/Berlin");
    }

    /**
     * Convert a date to a date time string using Coordinated Universal Time
     * (UTC) as time zone. Using a fixed time zone is important to display this
     * data independant from the user's time zone
     *
     * @param date
     *            the date to convert
     * @return the converted date time string, formatted as
     *         "yyyy-MM-dd'T'HH:mm:ss'Z'"
     */
    public static String convertDateToSecuredTimeString(Date date) {
        return convertToDateString(date, "yyyy-MM-dd'T'HH:mm:ss'Z'", "UTC");
    }

    /**
     * Convert a date time string to a date using Coordinated Universal Time
     * (UTC) as time zone. Using a fixed time zone is important to display this
     * data independant from the user's time zone
     *
     * @param value
     *            a date time string in format "yyyy-MM-dd'T'HH:mm:ss'Z'"
     * @return the date
     */
    public static Date convertSecuredTimeStringToDate(String value) {
        return convertFromDateString(value, "yyyy-MM-dd'T'HH:mm:ss'Z'", "UTC");
    }

    /**
     * Convert from date string to date.
     *
     * @param value            a date string in format "yyyy-MM-dd"
     * @param pattern    the pattern of the date string
     * @param timeZone   the timezone of the date string
     * @return the converted date, or the given date string if conversion failed
     */
    public static Date convertFromDateString(String value, String pattern, String timeZone) {
        SimpleDateFormat format;
        format = new SimpleDateFormat(pattern);
        format.setTimeZone(TimeZone.getTimeZone(timeZone));

        try {
            Date myDate = format.parse(value);
            return myDate;

        } catch (ParseException e) {
        }
        return null;
    }

    /**
     * Converts date to date string.
     *
     * @param date  the date
     * @param pattern   the pattern to transform to
     * @param timeZone  the timezone used
     * @return the string
     */
    public static String convertToDateString(Date date, String pattern, String timeZone) {
        SimpleDateFormat format;
        format = new SimpleDateFormat(pattern);
        format.setTimeZone(TimeZone.getTimeZone(timeZone));
        return format.format(date);
    }
}
