/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import de.samply.edc.utils.Utils;

/**
 * Converter to take care of German-English mixture for number typing (aka the , and . problem)
 *
 */
public class ConvertDouble implements Converter{

    /* (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component,
        String value) {

        if(value==null || value.equals(""))
            return null;

        Double minval = null, maxval = null;

        try{
            minval = Double.parseDouble((String) component.getAttributes().get("minval"));
            maxval = Double.parseDouble((String) component.getAttributes().get("maxval"));
        }
        catch(Exception e)
        {
        }
        if(minval == null)
            minval = -1000000000.0;
        if(maxval == null)
            maxval = 1000000000.0;

        try {
            Double test = Double.parseDouble(value);
            return test;
        } catch (Exception e)
        {
            value = value.replace(",", ".");
        }

        try {
            Double test = Double.parseDouble(value);
            return test;
        } catch (Exception e)
        {
            String wrong_range = Utils.getResourceBundleString("convert_double_no_value_error");
            String summary = Utils.getResourceBundleString("validation_error_header");

            FacesMessage msg = new FacesMessage(summary, wrong_range);
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ConverterException(msg);
        }

    }

    /* (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component,
            Object value) {
        if(value==null)
            return null;

        return value.toString();

    }
}