/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.converter;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import de.samply.edc.model.ICDO3t;

/**
 * class to convert ICDO3t to text.
 */
public class ConvertICDO3t implements Converter {

    /** The icdo3t db. */
    public static List<ICDO3t> icdo3tDB;

    static {
        icdo3tDB = new ArrayList<ICDO3t>();

        icdo3tDB.add(new ICDO3t(
                "C40.0",
                "Bösartige Neubildung des Knochens und des Gelenkknorpels der Extremitäten: Skapula und lange Knochen der oberen Extremität"));
        icdo3tDB.add(new ICDO3t(
                "C40.1",
                "Bösartige Neubildung des Knochens und des Gelenkknorpels der Extremitäten: Kurze Knochen der oberen Extremität"));
        icdo3tDB.add(new ICDO3t(
                "C40.2",
                "Bösartige Neubildung des Knochens und des Gelenkknorpels der Extremitäten: Lange Knochen der unteren Extremität"));
        icdo3tDB.add(new ICDO3t(
                "C40.3",
                "Bösartige Neubildung des Knochens und des Gelenkknorpels der Extremitäten: Kurze Knochen der unteren Extremität"));
        icdo3tDB.add(new ICDO3t(
                "C40.8",
                "Bösartige Neubildung des Knochens und des Gelenkknorpels der Extremitäten: Knochen und Gelenkknorpel der Extremitäten, mehrere Teilbereiche überlappend"));
        icdo3tDB.add(new ICDO3t(
                "C40.9",
                "Bösartige Neubildung des Knochens und des Gelenkknorpels der Extremitäten: Knochen und Gelenkknorpel einer Extremität, nicht näher bezeichnet"));
        icdo3tDB.add(new ICDO3t(
                "C41.0",
                "Bösartige Neubildung des Knochens und des Gelenkknorpels: Knochen des Hirn- und Gesichtsschädels"));
        icdo3tDB.add(new ICDO3t("C41.1",
                "Bösartige Neubildung des Knochens und des Gelenkknorpels: Unterkieferknochen"));
        icdo3tDB.add(new ICDO3t("C41.2",
                "Bösartige Neubildung des Knochens und des Gelenkknorpels: Wirbelsäule"));
        icdo3tDB.add(new ICDO3t(
                "C41.3",
                "Bösartige Neubildung des Knochens und des Gelenkknorpels: Rippen, Sternum und Klavikula"));
        icdo3tDB.add(new ICDO3t("C41.4",
                "Bösartige Neubildung des Knochens und des Gelenkknorpels: Beckenknochen"));
        icdo3tDB.add(new ICDO3t(
                "C41.8",
                "Bösartige Neubildung: Knochen und Gelenkknorpel, mehrere Teilbereiche überlappend"));
        icdo3tDB.add(new ICDO3t("C41.9",
                "Bösartige Neubildung: Knochen und Gelenkknorpel, nicht näher bezeichnet"));
        icdo3tDB.add(new ICDO3t("C46.0", "Kaposi-Sarkom der Haut"));
        icdo3tDB.add(new ICDO3t("C46.1", "Kaposi-Sarkom des Weichteilgewebes"));
        icdo3tDB.add(new ICDO3t("C46.2", "Kaposi-Sarkom des Gaumens"));
        icdo3tDB.add(new ICDO3t("C46.3", "Kaposi-Sarkom der Lymphknoten"));
        icdo3tDB.add(new ICDO3t("C46.7",
                "Kaposi-Sarkom sonstiger Lokalisationen"));
        icdo3tDB.add(new ICDO3t("C46.8", "Kaposi-Sarkom mehrerer Organe"));
        icdo3tDB.add(new ICDO3t("C46.9",
                "Kaposi-Sarkom, nicht näher bezeichnet"));
        icdo3tDB.add(new ICDO3t(
                "C47.0",
                "Bösartige Neubildung: Periphere Nerven des Kopfes, des Gesichtes und des Halses"));
        icdo3tDB.add(new ICDO3t(
                "C47.1",
                "Bösartige Neubildung: Periphere Nerven der oberen Extremität, einschließlich Schulter"));
        icdo3tDB.add(new ICDO3t(
                "C47.2",
                "Bösartige Neubildung: Periphere Nerven der unteren Extremität, einschließlich Hüfte"));
        icdo3tDB.add(new ICDO3t("C47.3",
                "Bösartige Neubildung: Periphere Nerven des Thorax"));
        icdo3tDB.add(new ICDO3t("C47.4",
                "Bösartige Neubildung: Periphere Nerven des Abdomens"));
        icdo3tDB.add(new ICDO3t("C47.5",
                "Bösartige Neubildung: Periphere Nerven des Beckens"));
        icdo3tDB.add(new ICDO3t("C47.6",
                "Bösartige Neubildung: Periphere Nerven des Rumpfes, nicht näher bezeichnet"));
        icdo3tDB.add(new ICDO3t(
                "C47.8",
                "Bösartige Neubildung: Periphere Nerven und autonomes Nervensystem, mehrere Teilbereiche überlappend"));
        icdo3tDB.add(new ICDO3t(
                "C47.9",
                "Bösartige Neubildung: Periphere Nerven und autonomes Nervensystem, nicht näher bezeichnet"));
        icdo3tDB.add(new ICDO3t("C48.0",
                "Bösartige Neubildung: Retroperitoneum"));
        icdo3tDB.add(new ICDO3t("C48.1",
                "Bösartige Neubildung: Näher bezeichnete Teile des Peritoneums"));
        icdo3tDB.add(new ICDO3t("C48.2",
                "Bösartige Neubildung: Peritoneum, nicht näher bezeichnet"));
        icdo3tDB.add(new ICDO3t(
                "C48.8",
                "Bösartige Neubildung: Retroperitoneum und Peritoneum, mehrere Teilbereiche überlappend"));
        icdo3tDB.add(new ICDO3t(
                "C49.0",
                "Bösartige Neubildung: Bindegewebe und andere Weichteilgewebe des Kopfes, des Gesichtes und des Halses"));
        icdo3tDB.add(new ICDO3t(
                "C49.1",
                "Bösartige Neubildung: Bindegewebe und andere Weichteilgewebe der oberen Extremität, einschließlich Schulter"));
        icdo3tDB.add(new ICDO3t(
                "C49.2",
                "Bösartige Neubildung: Bindegewebe und andere Weichteilgewebe der unteren Extremität, einschließlich Hüfte"));
        icdo3tDB.add(new ICDO3t("C49.3",
                "Bösartige Neubildung: Bindegewebe und andere Weichteilgewebe des Thorax"));
        icdo3tDB.add(new ICDO3t("C49.4",
                "Bösartige Neubildung: Bindegewebe und andere Weichteilgewebe des Abdomens"));
        icdo3tDB.add(new ICDO3t("C49.5",
                "Bösartige Neubildung: Bindegewebe und andere Weichteilgewebe des Beckens"));
        icdo3tDB.add(new ICDO3t(
                "C49.6",
                "Bösartige Neubildung: Bindegewebe und andere Weichteilgewebe des Rumpfes, nicht näher bezeichnet"));
        icdo3tDB.add(new ICDO3t(
                "C49.8",
                "Bösartige Neubildung: Bindegewebe und andere Weichteilgewebe, mehrere Teilbereiche überlappend"));
        icdo3tDB.add(new ICDO3t(
                "C49.9",
                "Bösartige Neubildung: Bindegewebe und andere Weichteilgewebe, nicht näher bezeichnet"));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext
     * , javax.faces.component.UIComponent, java.lang.String)
     */
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component,
            String submittedValue) {
        if (submittedValue.trim().equals("")) {
            return null;
        } else {
            try {
                for (ICDO3t i : icdo3tDB) {
                    if (i.getCode().equalsIgnoreCase(submittedValue)) {
                        return i;
                    }
                }

            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(
                        FacesMessage.SEVERITY_ERROR, "Conversion Error",
                        "Not a valid icd10 code"));
            }
        }
        return new ICDO3t(submittedValue); // TODO - accepts unknown codes, fix
                                            // when catalog is present
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext
     * , javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext facesContext, UIComponent component,
            Object value) {
        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((ICDO3t) value).getCode());
        }
    }
}