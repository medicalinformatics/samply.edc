/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.validator;

import java.text.MessageFormat;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import de.samply.edc.utils.Utils;

/**
 * JSF validator for integers within a certain range. The minimum and maximum 
 * values are given by the component's attributes 'minval' and 'maxval'.
 */
public class Range implements Validator {

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.faces.validator.Validator#validate(javax.faces.context.FacesContext
     * , javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) throws ValidatorException {
        String entry = (String) value;

        Integer minval = null, maxval = null;

        try {
            minval = Integer.parseInt((String) component.getAttributes().get(
                    "minval"));
            maxval = Integer.parseInt((String) component.getAttributes().get(
                    "maxval"));
        } catch (Exception e) {
        }
        if (minval == null)
            minval = -1000000000;
        if (maxval == null)
            maxval = 1000000000;

        String summary = Utils
                .getResourceBundleString("validation_error_header");
        String message = Utils.getResourceBundleString("validate_range_error");
        Locale locale = FacesContext.getCurrentInstance().getViewRoot()
                .getLocale();
        MessageFormat mf = new MessageFormat(message, locale);
        String[] mfromto = { minval.toString(), maxval.toString() };
        message = mf.format(mfromto, new StringBuffer(), null).toString();

        Integer number;

        try {
            number = Integer.parseInt(entry);
        } catch (Exception e) {
            FacesMessage msg = new FacesMessage(summary, message);
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }

        if (number < minval || number > maxval) {
            FacesMessage msg = new FacesMessage(summary, message);
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }

    }

}
