# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## 1.4.0 - 2017-08-29
### Added
- Possibility to copy the entire previous episode's values from all longitudinal forms.

### Changed
- Improved loading times by using a caching mechanism.
- Updated dependencies of several libraries to their latest versions.

### Fixed
- Fixed several i18n-related issues.